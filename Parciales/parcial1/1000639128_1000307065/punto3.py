import matplotlib.pyplot as plt
import numpy as np
from RKG import RKG_A

def lorentzEq(t,Y,sigma,rho,beta):
    """
    retorna un array con [dx/dt,dy/dt,dz/dt]
    """
    x,y,z = Y
    
    return np.array([sigma*(y-x),x*(rho-z)-y,x*y-beta*z])

cases = [{'ci':[0,0,0],'params':[10,28,8/3]},
         {'ci':[0,0.01,0],'params':[10,28,8/3]},
         {'ci':[0,0,0],'params':[16,45,4]},
         {'ci':[0,0.001,0],'params':[16,45,4]}]

fig = plt.figure(constrained_layout=False,figsize = (15,5))

ax = fig.add_gridspec(1,5)
ax1  = fig.add_subplot(ax[0])
ax2 = fig.add_subplot(ax[1])
ax3 = fig.add_subplot(ax[2])
ax4 = fig.add_subplot(ax[3:],projection = '3d')

solutions = []
for i,case in enumerate(cases):
    #la solución del caso 1 y 3 son cero debido a las condiciones iniciales, en las gráficas sólo aparecen 2 y 4.
    initialCon = case['ci']
    params = case['params']
    sols = RKG_A(lambda t,Y:lorentzEq(t,Y,*params),0,initialCon,100,0.001)
    x = sols.sol().transpose()[0]
    y = sols.sol().transpose()[1]
    z = sols.sol().transpose()[2]
    solutions.append(sols) #guarda las soluciones de cada caso en caso de necesitarse despues

    ax1.plot(y,x)
    ax2.plot(z,x)
    ax3.plot(z,y)
    ax4.plot(x,y,z,label = f'Caso {i+1}: {initialCon}, {[round(param,2) for param in params]}')

#---------------------
#Apariencia del gráfico
#---------------------
ax1.set_xlabel('y')
ax1.set_ylabel('x')

ax2.set_xlabel('z')
ax2.set_ylabel('x')

ax3.set_xlabel('z')
ax3.set_ylabel('y')

ax4.set_xlabel('x')
ax4.set_ylabel('y')
ax4.set_zlabel('z')
ax4.zaxis._axinfo['juggled'] = (1, 2, 2) #pasar la z al otro lado

fig.suptitle('Solución ecuación de Lorentz para 4 casos')

plt.tight_layout()
ax4.legend(title = 'Condiciones iniciales y parámetros',loc = 'upper right')
plt.savefig('punto3.png')


